#include "alimony.hpp"

Alimony::Alimony() {}

float Alimony::giveAmount()
{
    return initialAmount;
}
float Alimony::giveIndice()
{
    return initialIndice;
}
QDate Alimony::giveDecisionDate()
{
    return decisionDate;
}
void Alimony::setDecisionDate(QDate date)
{
    decisionDate.setDate(date.year(), date.month(), date.day());
}
float Alimony::reevaluateAmount(float lastIndice)
{
    float initialAmount,
        currentAmount,initialIndice;

    initialAmount=giveAmount();
    initialIndice=giveIndice();
    //grab lastIndice from csv file
    currentAmount=initialAmount*lastIndice/initialIndice;
    // qDebug()<<"lastIndice:"<<lastIndice<<
    //     " initialIndice:"<<initialIndice<<
    //     " initialAmount:"<<initialAmount<<
    //     " currentamount:"<<currentAmount;
    return currentAmount;
}
void Alimony::setInitialAmount(float amount)
{
    initialAmount=amount;
}
void Alimony::setInitialIndice(float indice)
{
    initialIndice=indice;
}
