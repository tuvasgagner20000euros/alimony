#include "mainwindow.hpp"
#include "./ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    ,m_alimony(new Alimony)
    ,m_manager(new csvManager)
{
    ui->setupUi(this);
    setCurrentDate();
    QFile file("./valeurs_mensuelles.csv");
    m_manager->loadDatas(file, m_dates, m_indices);
    file.close();
    qDebug()<<m_dates.size();
    connect(ui->calculate,&QPushButton::clicked, this, &MainWindow::setAlimony );
    connect(this, SIGNAL(datasValidated()), this, SLOT(extractDatas()));
    connect(ui->actionQuitter, &QAction::triggered, this, &MainWindow::close );
}

MainWindow::~MainWindow()
{
    disconnect(ui->calculate, &QPushButton::clicked, this, &MainWindow::setAlimony);
    disconnect(this, SIGNAL(datasValidated()), this, SLOT(extractDatas()));
    m_indices.clear();
    m_extractedDates.clear();
    m_extractedIndices.clear();
    m_dates.clear();
    delete ui;
    delete m_alimony;
    delete m_manager;
}
void MainWindow::setCurrentDate()
{
    QString text;
    //set currentDate
    now.setDate(QDate::currentDate().year(),QDate::currentDate().month(),
                QDate::currentDate().day());
    //then print
    text=now.toString("dd-MM-yyyy");
    ui->datePrint->setText(text);
    ui->datePrint->setEnabled(false);
}
float MainWindow::getNewAlimony(float newIndice)
{
    return m_alimony->reevaluateAmount(newIndice);
}
void MainWindow::setAlimony()
{
    qDebug()<<ui->startDate->date();
    m_alimony->setDecisionDate(ui->startDate->date());
    m_alimony->setInitialAmount(ui->startAmount->value());
    emit datasValidated();

}
void MainWindow::printTable()
{
    ui->tableWidget->clearContents();
    ui->tableWidget->clear();

    ui->tableWidget->setRowCount(m_extractedDates.size());
    ui->tableWidget->setColumnCount(3);
    QStringList list;
    list<<"Date"<<"Indice"<<"Montant";
    ui->tableWidget->setHorizontalHeaderLabels(list);

    for(int i=0;i<m_extractedDates.size();i++)
    {
        QTableWidgetItem* date = new QTableWidgetItem(m_extractedDates[i].toString("MM-yyyy"));
        QTableWidgetItem* indice = new QTableWidgetItem(QString::number(m_extractedIndices[i]));
        QTableWidgetItem* amount = new QTableWidgetItem(QString::number(getNewAlimony(m_extractedIndices[i])));

        ui->tableWidget->setItem(i,0,date);
        ui->tableWidget->setItem(i,1,indice);
        ui->tableWidget->setItem(i,2,amount);
    }
    ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
}
void MainWindow::extractDatas()
{
    // qDebug()<<"datasValidated!";
    m_extractedDates.clear();
    m_extractedIndices.clear();
    // qDebug()<<"amount:"<<m_alimony->giveAmount()
    //          <<" date:"<<m_alimony->giveDecisionDate();
    QDate initialDate = m_alimony->giveDecisionDate();
    //grab datas of interest
    for(int i=0;i<m_indices.size();i++)
    {
        if((m_dates[i].year()>=initialDate.year())
            &&(m_dates[i].month()==initialDate.month()))
        {
            m_extractedDates.push_back(m_dates[i]);
            m_extractedIndices.push_back(m_indices[i]);
        }
    }
    m_alimony->setInitialIndice(m_extractedIndices[m_extractedIndices.size()-1]);
    // qDebug()<<"dates size:"<<m_extractedDates.size()
    //          <<" indices size: "<<m_extractedIndices.size();
    // qDebug()<<m_extractedDates;
    // qDebug()<<m_extractedIndices;
    printTable();

}
