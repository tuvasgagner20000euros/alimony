#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>
#include <QString>
#include "alimony.hpp"
#include "csvmanager.hpp"
#include <QVector>
#include <QDate>
#include <QTableWidget>


QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void setCurrentDate();
    float getNewAlimony(float newIndice);
    void printTable();
    void setAlimony();

signals:
    void datasValidated();

public slots:
    void extractDatas();

private:
    Ui::MainWindow *ui;
    QDate now;
    Alimony* m_alimony;
    csvManager* m_manager;
    QVector<QDate> m_dates;
    QVector<float> m_indices;
    QVector<float> m_extractedIndices;
    QVector<QDate> m_extractedDates;
};
#endif // MAINWINDOW_HPP
