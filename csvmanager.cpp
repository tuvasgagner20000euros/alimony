#include "csvmanager.hpp"

csvManager::csvManager() {}

int csvManager::linesCount(QFile &file)
{
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return 0;

    int linesCount = 0;
    QTextStream in(&file);
    while (!in.atEnd()) {
        in.readLine();
        ++linesCount;
    }
    file.close();
    return linesCount;
}
void csvManager::loadDatas(QFile &file, QVector<QDate> &dates, QVector<float> &indices)
{
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug() << "Impossible d'ouvrir le fichier pour charger les données.";
        return;
    }
    else {

        QTextStream in(&file);
        int count=0;
        dates.clear();
        indices.clear();
        while (!in.atEnd()) {

                QString line = in.readLine();
                QStringList fields = line.split(";");

                if (fields.size() == 4) {
                    QString string = fields[0].mid(1, fields[0].size() - 2);//supprimer guillemets
                    QString floatString = fields[1].mid(1, fields[1].size() - 2);//supprimer guillemets
                    floatString.replace(',', '.');
                    qDebug()<<string<<floatString;
                    QDate date = QDate::fromString(string,"yyyy-MM");
                    date.setDate(date.year(), date.month(), 1);

                    if (!date.isValid()) {
                        qDebug() << "Erreur de format à la ligne" << count << ": Format de date invalide.";
                        continue; // Passe à la ligne suivante
                    }

                    bool ok;
                    float indice=floatString.toFloat(&ok);
                    if (!ok) {
                        qDebug() << "Erreur de format à la ligne" << count << ": Indice invalide.";
                        continue; // Passe à la ligne suivante
                    }

                    dates.push_back(date);
                    indices.push_back(indice);
                }
                count++;

        }
    }
    file.close();
}
