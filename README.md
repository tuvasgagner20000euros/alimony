# Alimony

Alimony is a brief application dedicated to calculate and reassess an alimony in France.


## Installation


- Clone the project and make the build.
- Download the csv file from Insee [here](https://www.insee.fr/fr/statistiques/serie/001763852)
and copy it into your build's repository.
