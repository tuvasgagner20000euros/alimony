#ifndef CSVMANAGER_HPP
#define CSVMANAGER_HPP

#include <QFile>
#include <QStringList>
#include <QString>
#include <QVector>
#include <QDate>

class csvManager
{
public:
    csvManager();
    int linesCount(QFile &file);
    void loadDatas(QFile &file, QVector<QDate> &dates, QVector<float> &indices);

private:

};

#endif // CSVMANAGER_HPP
