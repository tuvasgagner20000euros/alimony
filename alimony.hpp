#ifndef ALIMONY_HPP
#define ALIMONY_HPP
#include <QDate>

class Alimony
{

public:
    Alimony();
    float giveAmount();
    float giveIndice();
    QDate giveDecisionDate();
    void setDecisionDate(QDate date);
    void setInitialAmount(float amount);
    float reevaluateAmount(float lastIndice);
    void setInitialIndice(float indice);
private:
    float initialAmount;
    float initialIndice;
    QDate decisionDate;

};


#endif // ALIMONY_HPP
